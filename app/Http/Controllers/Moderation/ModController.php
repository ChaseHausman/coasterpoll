<?php

namespace ChaseH\Http\Controllers\Moderation;

use Illuminate\Http\Request;
use ChaseH\Http\Controllers\Controller;

class ModController extends Controller
{
    public function index(Request $request) {
        return view('mod.index');
    }
}
