<?php

namespace ChaseH\Http\Controllers\Moderation;

use ChaseH\Jobs\UpdateBallotStats;
use ChaseH\Models\Coasters\Coaster;
use ChaseH\Models\Coasters\Rank;
use ChaseH\Models\User;
use Illuminate\Http\Request;
use ChaseH\Http\Controllers\Controller;
use Illuminate\Support\Facades\Cache;

class BallotController extends Controller
{
    public function index(Request $request) {
        return view('mod.ballots.index');
    }

    public function list(Request $request) {
        $ids = Rank::select('user_id')->where('ballot_complete', 1)->distinct('user_id')->get();
        $user_ids = array_pluck($ids, 'user_id');

        $ballots = User::whereIn('id', $user_ids)->with('ranked', 'ridden')->paginate();

        return view('mod.ballots.list', [
            'ballots' => $ballots,
        ]);
    }

    public function view(User $user, Request $request) {
        $user->load(['ranked' => function($query) {
            return $query->orderBy('rank', 'ASC');
        }, 'ranked.coaster']);

        $rankings = $user->ranked;

        foreach($rankings as $ranking) {
            UpdateBallotStats::dispatch($ranking->coaster);
        }

        return view('mod.ballots.view', [
            'user' => $user,
            'rankings' => $rankings,
        ]);
    }

    public function coaster(Coaster $coaster) {
        $rankings = Rank::where('coaster_id', $coaster->id)->where('ballot_complete', true)->with('user')->orderBy('rank', 'ASC')->get();

        return view('mod.ballots.coaster', [
            'coaster' => $coaster,
            'rankings' => $rankings,
        ]);
    }

    public function min(Coaster $coaster) {
        return self::coaster($coaster);
    }

    public function max(Coaster $coaster) {
        return self::coaster($coaster);
    }

    public function avg(Coaster $coaster) {
        return self::coaster($coaster);
    }
}
