<?php

namespace ChaseH\Http\Controllers;

use Carbon\Carbon;
use ChaseH\Models\Coasters\Coaster;
use ChaseH\Models\Coasters\Manufacturer;
use ChaseH\Models\Coasters\Park;
use ChaseH\Models\Coasters\Rank;
use ChaseH\Models\Misc;
use ChaseH\Models\User;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;

class AdminController extends Controller
{
    public function dashboard() {
        $counts = Cache::remember('admin_counts', 60, function() {
            return json_encode([
                'user_objects' => User::withTrashed()->count(),
                'user_active' => User::count(),
                'user_new' => User::where('updated_at', '>=', Carbon::now()->subWeeks(2))->count(),
                'coasters' => Coaster::count(),
                'manufacturers' => Manufacturer::count(),
                'parks' => Park::count(),
                'ridden' => DB::table('coaster_user')->count(),
                'ranked' => Rank::count(),
                'completed' => Rank::select('user_id')->where('ballot_complete', 1)->distinct('user_id')->get()->count(),
                'incompleted' => Rank::select('user_id')->where('ballot_complete', 0)->distinct('user_id')->get()->count(),
            ]);
        });

        $searches = Cache::remember('search_counts', 60, function() {
            $guzzle = new Client([
                'headers' => [
                    'X-Algolia-Application-Id' => config('scout.algolia.id'),
                    'X-Algolia-API-Key' => config('scout.algolia.monitoring'),
                ]
            ]);

            if(config('app.environment') != "production") {
                return [
                    'searches' => "n/a",
                    'operations' => "n/a",
                    'records' => "n/a",
                ];
            }

            // Search Operations
            $response = $guzzle->request("GET", "https://status.algolia.com/1/usage/*/period/month");

            if($response->getStatusCode() == 200) {
                $results = json_decode($response->getBody());
                $searches = 0;
                foreach($results->total_search_operations as $operation) {
                    $searches = $operation->v + $searches;
                }

                $operations = 0;
                foreach($results->total_operations as $operation) {
                    $operations = $operation->v + $operations;
                }

                $records = 0;
                foreach($results->records as $operation) {
                    $records = $operation->v;
                }
            } else {
                $searches = "n/a";
                $operations = "n/a";
                $records = "n/a";
            }

            return [
                'searches' => $searches,
                'operations' => $operations,
                'records' => $records,
            ];
        });

        try {
            $view = view('console', [
                'counts' => json_decode($counts),
                'searches' => $searches,
            ]);
        } catch (\ErrorException $e) {
            if(str_contains($e->getMessage(), "Undefined")) {
                Cache::forget('admin_counts');
                return redirect(route('console'));
            }

            return abort(500);
        }

        return $view;
    }

    public function search(Request $request) {
        if($request->input('q') == null) {
            return view('admin.search');
        }

        // Search for users
        $users = User::query()->where('name', 'like', ($request->input('q')))->orWhere('email', 'like', $request->input('q'))->get();

        // Gather results
        $results = collect($users);

        return view('admin.search', [
            'results' => $results,
            'query' => $request->input('q'),
        ]);
    }

    public function viewVotingControls() {
        $controls = Misc::where('key', 'rankingControls')->first();

        if($controls == null) {
            $controls = Misc::create([
                'key' => 'rankingControls',
                'value' => [
                    'active' => 1,
                    'closeAt' => "",
                    'closedMessage' => "",
                ]
            ]);
        }

        return view('admin.coasters.voting', [
            'active' => $controls->value['active'],
            'closeAt' => $controls->value['closeAt'],
            'closedMessage' => $controls->value['closedMessage'],
        ]);
    }

    public function editVotingControls(Request $request) {
        $this->validate($request, [
            'active' => 'present',
            'closeAt' => 'present',
            'closedMessage' => 'present',
        ]);

        $value = [
            'active' => $request->get('active'),
            'closeAt' => $request->get('closeAt'),
            'closedMessage' => $request->get('closedMessage'),
        ];

        $controls = Misc::where('key', 'rankingControls')->update([
            'value' => json_encode($value),
        ]);

        return back()->withSuccess("Successfully saved new voting options.");
    }
}
