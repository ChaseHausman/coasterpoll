<?php

namespace ChaseH\Http\Controllers;

use ChaseH\Models\Coasters\Rank;
use ChaseH\Models\Emails;
use ChaseH\Models\User;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Log;

class EmailedController extends Controller
{
    public function mailgunUnsubscribe(Request $request) {
        if(!$this->verifyWebhookSignature($request->get('timestamp'), $request->get('token'), $request->get('signature'))) {
            return abort(401);
        }

        try {
            $user = User::where('email', $request->get('recipient'))->firstOrFail();
        } catch (ModelNotFoundException $e) {
            Log::info("Received unsubscribe notification for a recipient we wouldn't have sent an email to. [{$request->get('recipient')}]");
            return abort(200);
        }

        $user->setPreference(['email_subscription' => "unsubscribed"]);

        return response('Okay!');
    }

    public function verifyWebhookSignature($timestamp, $token, $signature)
    {
        if (empty($timestamp) || empty($token) || empty($signature)) {
            return false;
        }
        $hmac = hash_hmac('sha256', $timestamp.$token, $this->apiKey);
        if (function_exists('hash_equals')) {
            // hash_equals is constant time, but will not be introduced until PHP 5.6
            return hash_equals($hmac, $signature);
        } else {
            return $hmac === $signature;
        }
    }

    public function completeBallot($b64_email, $b64_secret, Request $request) {
        try {
            $sent = Emails::where('recipient', base64_decode(urldecode($b64_email)))->where('secret', urlencode($b64_secret))->firstOrFail();
        } catch (ModelNotFoundException $e) {
            return redirect(route("home"))->withWarning("Unable to find your email code.");
        }

        try {
            $user = User::where('email', base64_decode(urldecode($b64_email)))->firstOrFail();
        } catch (ModelNotFoundException $e) {
            return redirect(route('home'))->withWarning("Unable to find you.");
        }

        Rank::where('user_id', $user->id)->update([
            'ballot_complete' => 1,
        ]);

        Cache::forget('unranked:'.$user->id);
        Cache::forget('ranked:'.$user->id);

        return redirect(route('home'))->withSuccess("Thanks! Your ballot has been marked complete.");
    }

    public function incompleteBallot($b64_email, $b64_secret, Request $request) {
        try {
            $sent = Emails::where('recipient', base64_decode(urldecode($b64_email)))->where('secret', urlencode($b64_secret))->firstOrFail();
        } catch (ModelNotFoundException $e) {
            return redirect(route("home"))->withWarning("Unable to find your email code.");
        }

        try {
            $user = User::where('email', base64_decode(urldecode($b64_email)))->firstOrFail();
        } catch (ModelNotFoundException $e) {
            return redirect(route('home'))->withWarning("Unable to find you.");
        }

        return redirect(route('coasters.rank'));
    }
}
