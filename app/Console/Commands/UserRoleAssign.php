<?php

namespace ChaseH\Console\Commands;

use ChaseH\Models\Role;
use ChaseH\Models\User;
use Illuminate\Console\Command;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class UserRoleAssign extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'user:role {role}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Allows to mass assign a role to all users.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
            $role = Role::where('name', $this->argument('role'))->firstOrFail();
        } catch (ModelNotFoundException $e) {
            $this->error("Cannot find role \"{$this->argument('role')}\".");
            die();
        }

        $users = User::whereDoesntHave('roles', function($query) use ($role) {
            $query->where('id', $role->id);
        })->get();

        $progress = $this->output->createProgressBar(count($users));

        foreach($users as $user) {
            $user->roles()->attach($role);

            $progress->advance();
        }

        $progress->finish();
    }
}
