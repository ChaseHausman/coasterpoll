<?php

namespace ChaseH\Console\Commands\Coasters;

use ChaseH\Models\Coasters\Coaster;
use ChaseH\Models\Coasters\Result;
use Illuminate\Console\Command;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\Log;

class RerankResults extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'coasters:rerank {group}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Re-numbers results already in the database.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // Clear out old data
        Result::where('group', $this->argument('group'))->update([
            'rank' => null,
        ]);

        // Have each clone go through and calculate a percentage based on the average score of its members
        $clones = Coaster::where('is_aggregate', 1)->get();

        foreach($clones as $clone) {
            Log::info("##### Reranking {$clone->name} #####");
            $aggregates = Coaster::where('aggregated_to', $clone->id)->get();

            $sum = 0;
            $count = 0;
            foreach($aggregates as $aggregate) {
                $aggregate_result = Result::where('group', $this->argument('group'))->where('coaster_id', $aggregate->id)->first();

                if($aggregate_result == null) {
                    Log::info("-- Skipping a coaster not in this result set.");
                } else {
                    Log::info("-- Adding {$aggregate_result->percentage}%");

                    // Only add to average if there's an actual score there.
                    if($aggregate_result->percentage > 0) {
                        $sum = $sum + $aggregate_result->percentage;
                        $count = $count + 1;
                    }
                }
            }

            if($count > 0) {
                $average = $sum / $count;

                Log::info("--- Averaging...: {$average}%");

                Result::where('coaster_id', $clone->id)->where('group', $this->argument('group'))->update([
                    'percentage' => $average,
                ]);
            } else {
                Log::info("--- Count was <= 0");
            }
        }

        $rows = Result::where('group', $this->argument('group'))->orderBy('percentage', 'DESC')->get();

        $position = 1;
        foreach($rows as $row) {
            if($row->coaster->aggregated_to !== null && $row->coaster->aggregated_to > 0 && !$row->coaster->is_aggregate) {
                $row->update([
                    'flags' => "Is a clone of another ranked coaster.",
                ]);
            }

            if($row->flags == null && $row->display) {
                $row->update([
                    'rank' => $position,
                ]);

                $position++;
            }
        }
    }
}
