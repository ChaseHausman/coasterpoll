<?php

namespace ChaseH\Models;

use Illuminate\Database\Eloquent\Model;

class Misc extends Model
{
    protected $fillable = [
        'key',
        'value'
    ];

    protected $casts = [
        'value' => 'array'
    ];
}
