<?php

namespace ChaseH\Models;

use Illuminate\Database\Eloquent\Model;

class Emails extends Model
{
    protected $table = "emails";

    protected $fillable = [
        'recipient',
        'type',
        'secret',
    ];
}
