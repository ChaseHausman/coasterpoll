<?php

namespace ChaseH\Jobs;

use ChaseH\Models\Coasters\Coaster;
use ChaseH\Models\Coasters\Rank;
use ChaseH\Models\Coasters\Result;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Log;

class UpdateBallotStats implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $coaster;

    /**
     * Create a new job instance.
     *
     * @param Coaster $coaster
     * @return void
     */
    public function __construct(Coaster $coaster)
    {
        $this->coaster = $coaster;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $rankings = Rank::select('rank')->where('coaster_id', $this->coaster->id)->where('ballot_complete', true)->get();

        $min = $rankings->min('rank');
        $max = $rankings->max('rank');
        $avg = $rankings->average('rank');

        Cache::put("coaster:{$this->coaster->id}:min", $min, 86400);
        Cache::put("coaster:{$this->coaster->id}:max", $max, 86400);
        Cache::put("coaster:{$this->coaster->id}:avg", number_format($avg, 3), 86400);

        $result = Result::where('coaster_id', $this->coaster->id)->orderBy('updated_at', 'DESC')->first();

        Cache::put("coaster:{$this->coaster->id}:polled", $result, 86400);
    }
}
