<?php

namespace ChaseH\Jobs\Coasters;

use Carbon\Carbon;
use ChaseH\Models\Coasters\Coaster;
use ChaseH\Models\Coasters\Result;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Log;

class ProcessOne implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $coaster;
    private $coaster_ids;
    private $group;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($coaster, $ids, $group)
    {
        $this->coaster = $coaster;
        $this->coaster_ids = $ids;
        $this->group = $group;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        // Find all the people who've ridden $coaster
        $riders = array_column($this->coaster->rankings->toArray(), 'user_id');

        // Now find all the coasters that those people have ridden
        $opponents = Coaster::select('id')->where('id', '!=', $this->coaster->id)->whereIn('id', $this->coaster_ids)->whereHas('rankings', function($q) use ($riders) {
            $lastYear = Carbon::now()->subYear();
            $q->whereIn('user_id', $riders)->where('ballot_complete', 1)->orWhere('updated_at', '>=', $lastYear);
        })->with(['rankings' => function($q) {
            $lastYear = Carbon::now()->subYear();
            $q->select('user_id', 'coaster_id', 'rank')->where('ballot_complete', 1)->orWhere('updated_at', '>=', $lastYear);
        }])->get();

        // Reset overall coaster wins/losses/ties
        $wins = 0;
        $losses = 0;
        $ties = 0;
        $above_sum = 0;
        $below_sum = 0;
        $equal_sum = 0;

        foreach($opponents as $opponent) {
            $partials = array();

            $above = 0;
            $below = 0;
            $equal = 0;

            // Get the rankings, indexed by voter
            foreach($this->coaster->rankings as $coaster_ranking) {
                $partials[$coaster_ranking->user_id]['coaster'] = $coaster_ranking->rank;
            }

            foreach($opponent->rankings as $opponent_ranking) {
                $partials[$opponent_ranking->user_id]['opponent'] = $opponent_ranking->rank;
            }

            // Go through all those rankings.
            foreach($partials as $partial) {
                if((isset($partial['coaster']) && isset($partial['opponent'])) && $partial['coaster'] > $partial['opponent']) {
                    $below++;
                    $below_sum++;
                } elseif(isset($partial['coaster']) && isset($partial['opponent']) && $partial['coaster'] == $partial['opponent']) {
                    $equal++;
                    $equal_sum++;
                } elseif(isset($partial['coaster']) && isset($partial['opponent']) && $partial['coaster'] < $partial['opponent']) {
                    $above++;
                    $above_sum++;
                }
            }

            if($above > $below) {
                $wins++;
            } elseif($above < $below) {
                $losses++;
            } else {
                $ties++;
            }
        }

        if($wins+$losses != 0) {
            $percentage = (($wins+($ties * .5))/($wins+$losses+$ties))*100;
            $flags = null;
        } else {
            $percentage = 0;
            $flags = "Inconclusive results - no wins or loses. This is usually a result of only one rider.";
        }

        if(count($riders) <= 12) {
            $flags = "Too few riders - Less than 12 riders";
        }

        Result::create([
            'coaster_id' => $this->coaster->id,
            'group' => $this->group,
            'percentage' => $percentage,
            'above' => (isset($above_sum)) ? $above_sum: 0,
            'below' => (isset($below_sum)) ? $below_sum: 0,
            'equal' => (isset($equal_sum)) ? $equal_sum: 0,
            'wins' => $wins,
            'losses' => $losses,
            'ties' => $ties,
            'flags' => (isset($flags)) ? $flags : null,
            'rank' => null,
        ]);

        Log::info("Finished coaster.");
    }
}
