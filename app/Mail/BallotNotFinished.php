<?php

namespace ChaseH\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Log;

class BallotNotFinished extends Mailable
{
    use Queueable, SerializesModels;

    public $top5;
    public $message;
    public $deadline;
    public $links;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($message, $top5, $deadline, $secret, $email)
    {
        $this->top5 = $top5;
        $this->message = $message;
        $this->deadline = $deadline;

        $this->links = [
            'b64_email' => $email,
            'b64_secret' => $secret,
        ];
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('emails.unfinished-ballot');
    }
}
