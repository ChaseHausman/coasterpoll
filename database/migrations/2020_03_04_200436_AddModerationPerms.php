<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddModerationPerms extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        \Illuminate\Support\Facades\DB::table('permissions')->insert([
            ['name' => "Can moderate", 'group' => 'Moderation', 'description' => "The ability to moderate in general and see moderator tools."],
            ['name' => "Can moderate ballots", 'group' => 'Moderation', 'description' => "The ability to view all users ballots."],
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
