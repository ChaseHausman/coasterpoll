<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModifyResults extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('results', function(Blueprint $table) {
            $table->tinyInteger('display')->default(1);
            $table->text('display_as')->nullable()->default(null);
            $table->tinyInteger('display_original')->default(1);
            $table->tinyInteger('display_ridden')->default(1);

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('results', function(Blueprint $table) {
            $table->dropColumn('display');
            $table->dropColumn('display_as');
            $table->dropColumn('display_original');
            $table->dropColumn('display_ridden');
        });
    }
}
