<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AggrigateCoasters extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('coasters', function(Blueprint $table) {
            $table->integer('park_id')->nullable()->change();
            $table->integer('manufacturer_id')->nullable()->change();

            $table->boolean('is_aggregate')->default(false);
            $table->integer('aggregated_to')->nullable()->default(null);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('coasters', function(Blueprint $table) {
            $table->dropColumn(['is_aggregate', 'aggregated_to']);
        });
    }
}
