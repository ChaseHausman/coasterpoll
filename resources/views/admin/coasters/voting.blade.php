@extends('layouts.admin')

@section('title')
    Voting Controls
@endsection

@section('content')
    <div class="row">
        <div class="offset-md-3 col-md-6">
            <div class="card card-block card-outline-info">
                <form action="{{ route('admin.coasters.voting.post') }}" method="post">
                    <div class="form-group">
                        <label for="active">Is Voting Active</label>
                        <select name="active" id="active" class="form-control">
                            <option value="0" @if($active == "0")selected="selected"@endif>Not Allowed</option>
                            <option value="1" @if($active == "1")selected="selected"@endif>Allowed</option>
                            <option value="soon" @if($active == "soon")selected="selected"@endif>Close At A Time</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="closeAt">Close Voting At</label>
                        <input type="datetime" name="closeAt" id="closeAt" value="{{ $closeAt ?? "" }}" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="closedMessage">Closed Message</label>
                        <textarea name="closedMessage" id="closedMessage" class="form-control" rows="8">{{ $closedMessage }}</textarea>
                    </div>
                    <div class="form-group">
                        {{ csrf_field() }}
                        <button type="submit" class="btn btn-primary">Save Changes</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('scripts')

@endsection