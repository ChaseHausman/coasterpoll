@extends('layouts.admin')

@section('title')
    Unfinished Ballot Emails
@endsection

@section('content')
    <form action="{{ route('admin.emailer.send', ['key' => 'unfinished-ballot']) }}" method="post">
        {{ csrf_field() }}
        <div class="row">
            <div class="col-md-6">
                <div class="card card-block">
                    <div class="form-group">
                        <label for="message">Message Body</label>
                        <textarea name="message" class="form-control" id="message"></textarea>
                    </div>
                    <div class="form-group">
                        <label for="deadline">Deadline</label>
                        <input type="text" class="form-control" name="deadline" id="deadline">
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-outline-success">Send</button>
                        <button type="submit" class="btn btn-outline-secondary" formaction="{{ route('admin.emailer.preview', ['key' => 'unfinished-ballot']) }}" formtarget="_blank">Preview</button>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="deliver_at">Start Sending At</label>
                    <input type="datetime-local" name="deliver_at" id="deliver_at" class="form-control" placeholder="ASAP">
                </div>
            </div>
        </div>
    </form>
@endsection

@section('scripts')

@endsection