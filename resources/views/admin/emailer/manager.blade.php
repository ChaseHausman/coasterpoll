@extends('layouts.admin')

@section('title')
    Email Manager
@endsection

@section('content')
    <div class="row">
        <div class="col-md-6 col-md-offset-2">
            <div class="card card-block">
                Targeted Lists:
                <ul>
                    <li><a href="{{ route('admin.emailer.specific', ['key' => "unfinished-ballot"]) }}">Unfinished Ballots</a></li>
                </ul>
            </div>
        </div>
    </div>
@endsection

@section('scripts')

@endsection