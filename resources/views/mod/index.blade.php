@extends('layouts.app')

@section('title')
    Moderation
@endsection

@section('content')
    <div class="row">
        <div class="col-md-6 offset-md-3">
            <p class="text-center"><em>There are no moderation tools available at this time.</em></p>
        </div>
    </div>
@endsection

@section('scripts')

@endsection