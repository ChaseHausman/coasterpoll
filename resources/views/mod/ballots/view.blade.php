@extends('layouts.app')

@section('title')
    View {{ $user->name }}'s Ballot
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            <h2>Individual Ballot View</h2>
            <table class="table table-sm table-striped">
                <thead>
                <tr>
                    <th>User Rank</th>
                    <th>Polled Rank</th>
                    <th>Coaster</th>
                    <th>Best</th>
                    <th>Worst</th>
                    <th>Avg</th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                    @foreach($rankings as $ranking)
                        <tr>
                            <td>{{ $ranking->rank }}</td>
                            <td class="tooltip" title="{{ \Illuminate\Support\Facades\Cache::get('coaster:'.$ranking->coaster->id.':polled')->comments ?? "None" }}">
                                {{ \Illuminate\Support\Facades\Cache::get('coaster:'.$ranking->coaster->id.':polled')->rank ?? "-" }}
                            </td>
                            <td><a href="{{ route('coasters.coaster.id', ['coaster' => $ranking->coaster->id]) }}">{{ $ranking->coaster->name }}</a></td>
                            <td><a href="{{ route('mod.ballots.coaster-min', ['coaster' => $ranking->coaster->id]) }}" class="link-unstyled">{{ \Illuminate\Support\Facades\Cache::get('coaster:'.$ranking->coaster->id.':min') }}</a></td>
                            <td><a href="{{ route('mod.ballots.coaster-max', ['coaster' => $ranking->coaster->id]) }}" class="link-unstyled">{{ \Illuminate\Support\Facades\Cache::get('coaster:'.$ranking->coaster->id.':max') }}</a></td>
                            <td><a href="{{ route('mod.ballots.coaster-avg', ['coaster' => $ranking->coaster->id]) }}" class="link-unstyled">{{ \Illuminate\Support\Facades\Cache::get('coaster:'.$ranking->coaster->id.':avg') }}</a></td>
                            <td><a href="{{ route('mod.ballots.coaster', ['coaster' => $ranking->coaster->id]) }}">View More</a></td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection

@section('scripts')

@endsection