@extends('layouts.app')

@section('title')
    All Rankings for {{ $coaster->name }}
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            <h2>Rankings for {{ $coaster->name }} <small>at {{ $coaster->park->name }}</small></h2>
            <table class="table table-striped">
                <thead>
                <tr>
                    <th>Rank</th>
                    <th>User</th>
                </tr>
                </thead>
                <tbody>
                @foreach($rankings as $ranking)
                    <tr>
                        <td>{{ $ranking->rank }}</td>
                        <td><a href="{{ route('mod.ballots.view', ['user' => $ranking->user_id]) }}">{{ $ranking->user->name ?? "-" }}</a></td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection

@section('scripts')

@endsection