@extends('layouts.app')

@section('title')
    Ballot Moderation
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            <h2>Ballot Moderation Tools</h2>
        </div>
        <div class="col-md-8">
            <ul class="nav nav-pills">
                <li class="nav-item">
                    <a href="{{ route('mod.ballots.list') }}" class="nav-link">List of Ballots</a>
                </li>
            </ul>
        </div>
    </div>
@endsection

@section('scripts')

@endsection