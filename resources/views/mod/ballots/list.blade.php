@extends('layouts.app')

@section('title')
    Ballot List
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            <h2>Ballot List</h2>
            <table class="table table-sm table-striped table-hover">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>User</th>
                        <th>Rankings</th>
                        <th>Ridden</th>
                        <th>Status</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($ballots as $ballot)
                        <tr>
                            <td><a href="{{ route('mod.ballots.view', ['user' => $ballot->id]) }}">#{{ $ballot->id }}</a></td>
                            <td><a href="{{ route('mod.ballots.view', ['user' => $ballot->id]) }}">{{ $ballot->name }}</a></td>
                            <td>{{ $ballot->ranked->count() }}</td>
                            <td>{{ $ballot->ridden->count() }}</td>
                            <td>
                                @if($ballot->ranked->first()->complete)
                                    Complete
                                @else
                                    Incomplete
                                @endif
                            </td>
                            <td>

                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
            {{ $ballots->links() }}
        </div>
    </div>
@endsection

@section('scripts')

@endsection