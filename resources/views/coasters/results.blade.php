@extends('layouts.app')

@section('title')
    {{ $page->name }}
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            <h2>{{ $page->name }}</h2>
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th>Rank</th>
                        <th>% Wins</th>
                        <th>Coaster</th>
                        <th>Park</th>
                        @auth
                        <td></td>
                        @endauth
                    </tr>
                </thead>
                <tbody>
                    @foreach($results as $result)
                        @if($result->display)
                            <tr @if($result->flags !== null) class="table-warning" @endif>
                                @if($result->flags !== null)
                                    <td title="Not Included" data-toggle="popover" data-content="{{ $result->flags }}">{{ $result->rank ?? "*" }}</td>
                                @elseif($result->comments !== null)
                                    <td title="Additional Info" data-toggle="popover" data-content="{{ $result->comments }}">{{ $result->rank }}&#8224;</td>
                                @else
                                    <td>{{ $result->rank }}</td>
                                @endif
                                <td>{{ $result->percentage }}%</td>
                                @if($result->display_original)
                                    <td><a href="{{ route('coasters.coaster', ['park' => $result->coaster->park->short, 'coaster' => $result->coaster->slug]) }}">{{ $result->coaster->name }}</a></td>
                                    <td><a href="{{ route('coasters.park', ['park' => $result->coaster->park->short]) }}">{{ $result->coaster->park->name }}</a></td>
                                @else
                                    <td colspan="2">{!! $result->display_as !!}</td>
                                @endif
                                @auth
                                <td>
                                    @if($result->display_ridden)
                                        @include('coasters._ridden', ['ridden_coaster_id' => $result->coaster_id])
                                    @endif
                                    @can('Can run results')
                                        <a href="{{ route('coasters.results.modify', ['result' => $result->id]) }}" class="btn btn-outline-secondary btn-sm">Modify</a>
                                    @endcan
                                </td>
                                @endauth
                            </tr>
                        @else
                            @can('Can run results')
                                <tr class="table-danger">
                                    <td @if($result->flags !== null) title="Not Included" data-toggle="popover" data-content="{{ $result->flags }}" @endif>{{ $result->rank ?? "*" }}</td>
                                    <td>{{ $result->percentage }}%</td>
                                    @if($result->display_original)
                                        <td><a href="{{ route('coasters.coaster', ['park' => $result->coaster->park->short, 'coaster' => $result->coaster->short]) }}">{{ $result->coaster->name }}</a></td>
                                        <td><a href="{{ route('coasters.park', ['park' => $result->coaster->park->short]) }}">{{ $result->coaster->park->name }}</a></td>
                                    @else
                                        <td colspan="2">{{ $result->display_as }}</td>
                                    @endif
                                    @auth
                                        <td>
                                            @if($result->display_ridden)
                                                @include('coasters._ridden', ['ridden_coaster_id' => $result->coaster_id])
                                            @endif
                                            @can('Can run results')
                                                <a href="{{ route('coasters.results.modify', ['result' => $result->id]) }}" class="btn btn-outline-secondary btn-sm">Modify</a>
                                            @endcan
                                        </td>
                                    @endauth
                                </tr>
                            @endcan
                        @endif
                    @endforeach
                </tbody>
            </table>
            {{ $results->links('vendor.pagination.bootstrap-4') }}
            <p>* Results not included in ranking.</p>
            <p>&#8224; Additional information is available by clicking on the number.</p>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        $(function () {
            $('[data-toggle="popover"]').popover()
        });
    </script>
@endsection