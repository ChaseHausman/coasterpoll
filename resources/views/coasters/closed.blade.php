@extends('layouts.app')

@section('title')
    Voting is Closed
@endsection

@section('content')
    <div class="row">
        <div class="offset-md-3 col-md-6">
            <div class="card card-outline-secondary">
                <div class="card-header">
                    <h2 class="card-title">Voting is Closed</h2>
                </div>
                <div class="card-block">
                    <p>{!! $message !!}</p>
                    <p>Your rankings are available to read/share on <a href="{{ route('profile.rankings', ['handle' => \Illuminate\Support\Facades\Auth::user()->handle]) }}">your profile</a>.</p>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')

@endsection