@extends('layouts.app')

@section('title')
Modify Result #{{ $result->id }}
@endsection

@section('content')
    <div class="row">
        <div class="col-md-10 offset-md-1">
            <h1>Result #{{ $result->id }} </h1>
            <h2>{{ $result->coaster->name }} in {{ $result->group }}</h2>
            <div class="card card-block">
                <form action="{{ route('coasters.results.modify.post', ['result' => $result->id]) }}" method="post">
                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="btn-group mr-2" data-toggle="buttons">
                                    <label class="btn btn-primary @if(!$result->display) active @endif">
                                        <input type="radio" name="display" value="0" id="display_yes" autocomplete="off" @if(!$result->display) checked @endif> Hide Entry
                                    </label>
                                    <label class="btn btn-primary @if($result->display) active @endif">
                                        <input type="radio" name="display" value="1" id="display_no" autocomplete="off" @if($result->display) checked @endif> Show Entry
                                    </label>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="btn-group mr-2" data-toggle="buttons">
                                    <label class="btn btn-primary @if(!$result->display_ridden) active @endif">
                                        <input type="radio" name="display_ridden" value="0" id="display_ridden_yes" autocomplete="off" @if(!$result->display_ridden) checked @endif> Hide Ridden
                                    </label>
                                    <label class="btn btn-primary @if($result->display_ridden) active @endif">
                                        <input type="radio" name="display_ridden" value="1" id="display_ridden_no" autocomplete="off" @if($result->display_ridden) checked @endif> Show Ridden
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-12">
                                <label for="display_as">Display As</label>
                                <input type="text" name="display_as" id="display_as" class="form-control" value="{{ $result->display_as ?? $result->coaster->name }}">
                                <div class="btn-group btn-group-sm mr-2 mt-4" data-toggle="buttons">
                                    <label class="btn btn-sm btn-primary @if(!$result->display_original) active @endif">
                                        <input type="radio" name="display_original" value="0" id="display_original_custom" autocomplete="off" @if(!$result->display_original) checked @endif> Custom Name
                                    </label>
                                    <label class="btn btn-sm btn-primary @if($result->display_original) active @endif">
                                        <input type="radio" name="display_original" value="1" id="display_original_name" autocomplete="off" @if($result->display_original) checked @endif> Coaster Name
                                    </label>
                                    <label class="btn btn-sm btn-secondary disabled">
                                        <input type="radio" disabled> ({{ $result->coaster->name }})
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-12">
                                <label for="flags">Flags</label>
                                <input type="text" name="flags" id="flags" class="form-control" value="{{ $result->flags }}">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-2">
                                <label for="rank">Rank</label>
                                <input type="number" name="rank" id="rank" value="{{ $result->rank }}" class="form-control">
                            </div>
                            <div class="col-md-2">
                                <label for="percentage">Percentage</label>
                                <input type="number" id="percentage" value="{{ $result->percentage }}" class="form-control" readonly>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-2">
                                <label for="wins">wins</label>
                                <input type="number" id="wins" value="{{ $result->wins }}" class="form-control" readonly>
                            </div>
                            <div class="col-md-2">
                                <label for="losses">losses</label>
                                <input type="number" id="losses" value="{{ $result->losses }}" class="form-control" readonly>
                            </div>
                            <div class="col-md-2">
                                <label for="ties">ties</label>
                                <input type="number" id="ties" value="{{ $result->ties }}" class="form-control" readonly>
                            </div>
                            <div class="col-md-2">
                                <label for="above">above</label>
                                <input type="number" id="above" value="{{ $result->above }}" class="form-control" readonly>
                            </div>
                            <div class="col-md-2">
                                <label for="below">below</label>
                                <input type="number" id="below" value="{{ $result->below }}" class="form-control" readonly>
                            </div>
                            <div class="col-md-2">
                                <label for="equal">equal</label>
                                <input type="number" id="equal" value="{{ $result->equal }}" class="form-control" readonly>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-12">
                                <label for="comments">Additional Comments</label>
                                <textarea name="comments" class="form-control" id="comments">{{ $result->comments }}</textarea>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-12">
                                {{ csrf_field() }}
                                <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Modify</button>
                                <label class="form-check-inline form-check-label">
                                    <input type="checkbox" name="force_rerank" value="1" id="force_rerank" class="form-check-input" autocomplete="off"> Rerank
                                </label>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('scripts')

@endsection